import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConcensusListPage } from './concensus-list.page';

const routes: Routes = [
  {
    path: '',
    component: ConcensusListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConcensusListPageRoutingModule {}
