import { Component, OnInit } from '@angular/core';
import {Concensus} from '../models/concensus';
import { ConcensusFakeDataService } from '../services/concensus-fake-data.service';
import {ToastController} from '@ionic/angular';
import {LocalConcensusService} from '../services/local-concensus.service';

@Component({
  selector: 'app-concensus-list',
  templateUrl: './concensus-list.page.html',
  styleUrls: ['./concensus-list.page.scss'],
})
export class ConcensusListPage implements OnInit {

    queryText = '';
    concensusList: Concensus [] = [];
    concensusByYearMap: Map<number, Concensus[]> = new Map<number, Concensus[]>();

    constructor(
        public concensusDataService: ConcensusFakeDataService,
        public localConcensusService: LocalConcensusService,
        public toastController: ToastController) { }

    ngOnInit() {
        this.initializeConcensusData();
        this.verifyConcensusLocalDataDate();
    }

    initializeConcensusData() {
        // Buscamos si existen datos locales
        this.localConcensusService.getLocalConcensus().then((concensusList) => {
            if (concensusList !== null) {
                // console.log('load local concensusList' + concensusList);
                this.concensusList = concensusList;

                this.generateConcensusByYearMap();
            } else {
                this.getRemoteConcensusListLoad();
            }
        }).catch(e => {
            this.presentToast('Error: ' + e, 1);
        });
    }

    getRemoteConcensusListLoad() {
        this.concensusDataService.getData().subscribe((data) => {
            this.concensusList = data.concensus;
            // console.log('set local concensusList' + data.concensus);
            this.localConcensusService.setLocalConcensus(this.concensusList).catch(e => {
                this.presentToast('Error: ' + e, 1);
            });
            this.localConcensusService.setConcensusDateUpdate().catch(e => {
                this.presentToast('Error: ' + e, 1);
            });

            this.generateConcensusByYearMap();
            // console.log(JSON.stringify(this.concensusList));
        });
    }

    verifyConcensusLocalDataDate() {
        this.localConcensusService.getConcensusDate().then((concensusDate) => {
            if (concensusDate !== null) {
                this.concensusDataService.getData().subscribe((data) => {
                    // console.log(this.parseData(data.concensusDataDate) + ' ' + concensusDate);
                    if (this.parseData(data.concensusDataDate) > this.parseData(concensusDate)) {
                       this.alertOutdateData();
                    }
                });
            }
        }).catch(e => {
            this.presentToast('Error: ' + e, 1);
        });
    }

    alertOutdateData() {
        this.presentToast('Datos Desactualizados.', 1);
    }

    private generateConcensusByYearMap() {
        let year: number = null;
        let concensusByYearAux: Concensus [] = [];
        if (this.concensusList !== null && this.concensusList.length > 0) {
            // Primero inicializamos el año
            if (this.concensusList[0].modificationDate != null) {
                this.concensusList[0].modificationDate = this.parseData(this.concensusList[0].modificationDate);
                year = this.concensusList[0].modificationDate.getFullYear();
            } else {
                this.concensusList[0].creationDate = this.parseData(this.concensusList[0].creationDate);
                year = this.concensusList[0].creationDate.getFullYear();
            }

            for (const concensus of this.concensusList) {
                // Si cambia el año, guardamos en el mapa y  actualizamos el año
                if (concensus.modificationDate != null) {
                    concensus.modificationDate = this.parseData(concensus.modificationDate);
                    if (year !== concensus.modificationDate.getFullYear()) {

                        this.concensusByYearMap.set(year, concensusByYearAux);
                        concensusByYearAux = [];
                        year = concensus.modificationDate.getFullYear();
                    }
                } else {
                    concensus.creationDate = this.parseData(concensus.creationDate);
                    if (year !== concensus.creationDate.getFullYear()) {
                        this.concensusByYearMap.set(year, concensusByYearAux);
                        concensusByYearAux  = [];
                        year = concensus.creationDate.getFullYear();
                    }
                }

                // Guardamos en una lista los concensos de este año
                concensusByYearAux.push(concensus);
            }

            this.concensusByYearMap.set(year, concensusByYearAux);

            /*
            for (const [key, value] of this.concensusByYearMap) {     // get data sorted
                console.log(key + ' ' + value);
            }
            console.log(this.concensusByYearMap);
            */

        }

    }

    updateConcensus() {
        this.generateConcensusByYearMap();

        const concensusByYearMapAx: Map<number, Concensus[]> = new Map<number, Concensus[]>();

        for (const [key, value] of this.concensusByYearMap) {// get data sorted
            // console.log(value);

            if (value) {
                const result = value.filter(concensusYear => {
                    if (concensusYear.title.toLowerCase().includes(this.queryText.toLowerCase()) ||
                        concensusYear.summary.toLowerCase().includes(this.queryText.toLowerCase()) ||
                        concensusYear.tags.toLowerCase().includes(this.queryText.toLowerCase()) ||
                        concensusYear.user.toLowerCase().includes(this.queryText.toLowerCase())) {
                        // console.log(concensusYear.title.toLowerCase() + ' ' + this.queryText.toLowerCase());
                        return concensusYear;
                    }
                });

                if (result !== null && result.length > 0) {
                    concensusByYearMapAx.set(key, result);
                }
            }
        }
        // console.log(concensusByYearMapAx);

        if (concensusByYearMapAx.size > 0) {
            this.concensusByYearMap = concensusByYearMapAx;
        }
    }

    originalOrder = (a, b): number => {
        return 0;
    }

    // Order by descending property key
    keyDescOrder = (a, b): number => {
        return a.key > b.key ? -1 : (b.key > a.key ? 1 : 0);
    }

    // TODO: quitar esto, cuando tengamos el servicio esto no se va a usar
    parseData(value) {
        const reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;

        const a = reISO.exec(value);
        // console.log(new Date(value))
        return new Date(value);
    }

    async presentToast(msg: string, state: number) {
        let colorAux = 'success';
        if (state === 1) {
            colorAux = 'danger';
        }
        const toast = await this.toastController.create({
            message: msg,
            color: colorAux,
            // showCloseButton: true,
            // closeButtonText: 'Cerrar',
            animated: true,
            duration: 4000
        });
        toast.present();
    }

}
