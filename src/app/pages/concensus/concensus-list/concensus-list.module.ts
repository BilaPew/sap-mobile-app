import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ConcensusListPageRoutingModule } from './concensus-list-routing.module';

import { ConcensusListPage } from './concensus-list.page';
import { ConcensusItemComponent } from './concensus-item/concensus-item.component';
import { ConcensusGroupComponent } from './concensus-group/concensus-group.component';
import { ConcensusFakeDataService } from '../services/concensus-fake-data.service';
import { LocalConcensusService } from '../services/local-concensus.service';
import { SharedModule } from '../../../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConcensusListPageRoutingModule,
    SharedModule
  ],
  declarations: [ConcensusListPage, ConcensusGroupComponent, ConcensusItemComponent],
  providers: [
    ConcensusFakeDataService,
    LocalConcensusService
  ]
})
export class ConcensusListPageModule {}
