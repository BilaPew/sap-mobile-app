import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConcensusListPage } from './concensus-list.page';

describe('ConcensusListPage', () => {
  let component: ConcensusListPage;
  let fixture: ComponentFixture<ConcensusListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConcensusListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConcensusListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
