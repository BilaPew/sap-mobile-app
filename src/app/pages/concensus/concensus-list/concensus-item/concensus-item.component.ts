import {Component, Input, OnInit} from '@angular/core';
import {Concensus} from '../../models/concensus';
import {ToastController} from '@ionic/angular';
import {LocalConcensusService} from '../../services/local-concensus.service';

@Component({
  selector: 'app-concensus-item',
  templateUrl: './concensus-item.component.html',
  styleUrls: ['./concensus-item.component.scss'],
})
export class ConcensusItemComponent implements OnInit {

  @Input()
  concensus: Concensus;
  public isMenuOpen = false;

  constructor(
      public toastController: ToastController,
      private localConcensusService: LocalConcensusService) { }

  ngOnInit() {
  }

  onFileSavedUrl(fileSavedPath: string) {
      // console.log('file save ' + fileSavedPath);
      // Update concensus in local concensus list
      // this.presentToast('La descarga se realizo con exito' + fileSavedPath, 0);

      this.localConcensusService.getLocalConcensus().then((concensusList) => {
          if (concensusList !== null) {
              //  this.presentToast('list ' + concensusList.size, 0);
          }
      });
      this.localConcensusService.getLocalConcensus().then((concensusList) => {
          if (concensusList !== null) {
              this.concensus.existLocalFile = true;
              // console.log('load local concensusList' + concensusList);
              const concensusListAux: Concensus[] = concensusList;

              const concensusListToSave: Concensus[] = [];

              for (let concensusAux of concensusListAux) {
                  if ( concensusAux.id === this.concensus.id ) {
                      concensusAux.existLocalFile = true;
                  }
                  concensusListToSave.push(concensusAux);
              }
              this.localConcensusService.setLocalConcensus(concensusListToSave).catch(e => {
                  this.presentToast('Error: ' + e, 1);
              });
              this.presentToast('La descarga se realizo con exito...', 0);
          }
      }).catch(e => {
          this.presentToast('Error: ' + e, 1);
      });


  }

  public toggleAccordion(): void {
    this.isMenuOpen = !this.isMenuOpen;
  }

    async presentToast(msg: string, state: number) {
        let colorAux = 'success';
        if (state === 1) {
            colorAux = 'danger';
        }
        const toast = await this.toastController.create({
            message: msg,
            color: colorAux,
            // showCloseButton: true,
            // closeButtonText: 'Cerrar',
            animated: true,
            duration: 3000
        });
        toast.present();
    }

}
