import {Component, Input, OnInit} from '@angular/core';
import {Concensus} from '../../models/concensus';

@Component({
  selector: 'app-concensus-group',
  templateUrl: './concensus-group.component.html',
  styleUrls: ['./concensus-group.component.scss'],
})
export class ConcensusGroupComponent implements OnInit {

  @Input()
  year: number;

  @Input()
  concensusList: Concensus [] = [];

  public isMenuOpen = false;

  constructor() { }

  ngOnInit() { }

  public toggleAccordion(): void {
    this.isMenuOpen = !this.isMenuOpen;
  }

}
