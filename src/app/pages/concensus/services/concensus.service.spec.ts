import { TestBed } from '@angular/core/testing';

import { ConcensusService } from './concensus.service';

describe('ConcensusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConcensusService = TestBed.get(ConcensusService);
    expect(service).toBeTruthy();
  });
});
