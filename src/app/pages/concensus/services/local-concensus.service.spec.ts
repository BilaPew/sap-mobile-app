import { TestBed } from '@angular/core/testing';

import { LocalConcensusService } from './local-concensus.service';

describe('LocalConcensusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocalConcensusService = TestBed.get(LocalConcensusService);
    expect(service).toBeTruthy();
  });
});
