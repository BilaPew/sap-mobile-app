import { TestBed } from '@angular/core/testing';

import { ConcensusFakeDataService } from './concensus-fake-data.service';

describe('ConcensusFakeDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConcensusFakeDataService = TestBed.get(ConcensusFakeDataService);
    expect(service).toBeTruthy();
  });
});
