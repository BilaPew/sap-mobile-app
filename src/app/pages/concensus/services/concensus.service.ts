import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Concensus} from '../models/concensus';

@Injectable({
  providedIn: 'root'
})
export class ConcensusService {

    concensusList: any;

    private url = environment.url.api.concensus;

    constructor(private http: HttpClient) {

    }


    getAll(): Observable<Concensus[]> {
        return this.http.get<Concensus[]>(`${this.url}`)
            .pipe(
                // tap(this.extractData),
                catchError(this.handleError)
            );
    }

    private handleError(error: HttpErrorResponse) {

        if (error.error instanceof ErrorEvent) {

            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);

        } else {

            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${'ERROR'}, ` +
                `body was: ${error.message}`);
        }

        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    }
}
