import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Concensus} from '../models/concensus';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConcensusFakeDataService {

  private dataUrl = 'assets/data/concensus.json';

  constructor(private http: HttpClient) { }

  getData(): Observable<any> {
    return this.http.get<any>(this.dataUrl);
  }

}
