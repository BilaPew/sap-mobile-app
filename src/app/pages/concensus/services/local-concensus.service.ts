import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage';
import {Concensus} from '../models/concensus';

@Injectable({
  providedIn: 'root'
})
export class LocalConcensusService {

  constructor(private storage: Storage) { }

  clearData() {
    this.storage.clear();
  }

  async getLocalConcensus(): Promise<any> {
      try {
          const result = await this.storage.get('concensusList');
          // console.log('storageGET: ' + 'concensusList' + ': ' + result);
          if (result != null) {
              return result;
          }
          return null;
      } catch (reason) {
          // console.log(reason);
          return null;
      }
  }


  async setLocalConcensus(concensusList: Concensus []) {
      try {
          const result = await this.storage.set('concensusList', concensusList);
          // console.log('set Object in storage: ' + result);
          return true;
      } catch (reason) {
          // console.log(reason);
          return false;
      }
  }

  async setConcensusDateUpdate() {
      try {
          const result = await this.storage.set('concensusDate', new Date());
          return true;
      } catch (reason) {
          // console.log(reason);
          return false;
      }
  }


  async getConcensusDate(): Promise<any> {
        try {
            const result = await this.storage.get('concensusDate');
            // console.log('storageGET: ' + 'concensusList' + ': ' + result);
            if (result != null) {
                return result;
            }
            return null;
        } catch (reason) {
            // console.log(reason);
            return null;
        }
    }
}
