export class Concensus {
    constructor(
        public id?: number,
        public type?: string,
        public title?: string,
        public user?: string,
        public creationDate?: Date,
        public modificationDate?: Date,
        public dischargeDate?: Date,
        public tags?: string,
        public summary?: string,
        public fileUrl?: string,
        public existLocalFile?: boolean
    ) {
    }
}
