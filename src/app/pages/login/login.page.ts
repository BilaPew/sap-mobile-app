import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {AuthenticationService} from '../../shared/services/authentication.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastController} from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginPage implements OnInit {

    formGroup: FormGroup;

    validations = {
        username: [
            { type: 'required', message: 'El usuario es requerido.' },
            { type: 'minlength', message: 'La longitud minima es 4 caracteres.' },
            { type: 'maxlength', message: 'La longitud maxima es 16 caracteres.' },
            { type: 'pattern', message: 'Debe contener solo letras y numeros.' }
        ],
        password: [
            { type: 'required', message: 'La contraseña es requerida.' },
            { type: 'minlength', message: 'La longitud minima es 4 caracteres.' },
            { type: 'maxlength', message: 'La longitud maxima es 16 caracteres.' },
            { type: 'pattern', message: 'Debe contener solo letras y numeros.' }
        ],
        // other validations
    };

    constructor(
        private authService: AuthenticationService,
        private formBuilder: FormBuilder,
        public toastController: ToastController
    ) { }

    ngOnInit() {
        this.createForm();
    }

    createForm(): void {
        this.formGroup = this.formBuilder.group({
            username: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(16)]),
            password: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(16)])
        });
    }

    // convenience getter for easy access to form fields
    get fg() {
        return this.formGroup.controls;
    }

    // fake onSubmit
    onSubmit() {
        // console.log(this.fg.username.value + ' ' + this.fg.password.value);
        if (this.authService.login({
            username: this.fg.username.value,
            password: this.fg.password.value
        })) {
            this.presentToast('Acceso concedido.', 0);
        } else {
            this.presentToast('Usuario o contraseña, incorrectos.', 1);
        }
    }

    // Real onSubmit
   /* onSubmit() {
        this.authService.login({
            username: this.fg.username.value,
            password: this.fg.password.value
        }).subscribe(
            data => {
                this._router.navigate([this.returnUrl]);
            },
            error => {
                if (error === 'Bad credentials') {
                    this._appToastService.error('Acceso denegado', 'Verifique los datos e intente nuevamente.');
                } else {
                    this._appToastService.error('Acceso', 'Ha ocurrido un error intente nuevamente.');
                }
            });
    }*/

    async presentToast(msg: string, state: number) {
        let colorAux = 'success';
        if (state === 1) {
            colorAux = 'danger';
        }
        const toast = await this.toastController.create({
            message: msg,
            color: colorAux,
            // showCloseButton: true,
            // closeButtonText: 'Cerrar',
            animated: true,
            duration: 1500
        });
        toast.present();
    }
}
