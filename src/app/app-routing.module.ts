import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './shared/services/auth-guard.service';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule) },
    { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule) },
    {
        path: 'config',
        loadChildren: () => import('./pages/configuration/configuration.module').then( m => m.ConfigurationPageModule),
        canActivate: [AuthGuardService]
    },
    {
        path: 'concensus-list',
        loadChildren: () => import('./pages/concensus/concensus-list/concensus-list.module').then(m => m.ConcensusListPageModule)
    }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
