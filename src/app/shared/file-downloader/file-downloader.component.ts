import {Component, EventEmitter, Input, OnInit, Output, OnDestroy} from '@angular/core';
import { ToastController, AlertController, LoadingController } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-file-downloader',
  templateUrl: './file-downloader.component.html',
  styleUrls: ['./file-downloader.component.scss'],
})
export class FileDownloaderComponent implements OnInit, OnDestroy {

    @Input()
    fileName: string;
    @Input()
    downloadUrl: string;
    @Input()
    folder: string;

    @Output()
    fileSavedUrl = new EventEmitter<string>();

    wifiOn = false;
    private connectSubscription$: Subscription = null;

    loaderToShow: any;

    constructor(
        private file: File,
        private fileTransfer: FileTransfer,
        private network: Network,
        public toastController: ToastController,
        public alertController: AlertController,
        public loadingController: LoadingController
    ) { }

    ngOnInit() {
       this.WatchConnection();
    }

    WatchConnection() {
        if (this.connectSubscription$) { this.connectSubscription$.unsubscribe(); }
        this.connectSubscription$ = this.network.onDisconnect().subscribe(() => {
            this.presentToast('Al parece no existe conexion a internet: verifique su configuracion', 1);
          if (this.connectSubscription$) { this.connectSubscription$.unsubscribe(); }
          this.connectSubscription$ = this.network.onConnect().subscribe(() => {
            setTimeout(() => {
              if (this.network.type === 'wifi'){ /* || this.network.type === '4g' || this.network.type === '3g' || this.network.type === '2g' || this.network.type === 'cellular' || this.network.type === 'none') { */
                this.wifiOn = true;
                this.WatchConnection();
              } else {
                this.wifiOn = false;
              }
            }, 3000);
          });
        });
      }
    

    async presentLoading() {
        const loading = await this.loadingController.create({
          message: 'Aguarde por favor',
          duration: 2000
        });
        await loading.present();

        const { role, data } = await loading.onDidDismiss();

        console.log('Loading dismissed!');
    }

    async presentAlertConfirm() {
        if(this.wifiOn){
            this.downloadPDF();
        } else {
            const alert = await this.alertController.create({
                header: 'Advertencia De Uso de Datos',
                message: ' <ion-icon name="warning"></ion-icon> <strong>Para evitar cargos adicionales, se recomienda el uso de Wi-Fi</strong>',
                buttons: [
                  {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                      console.log('Cancel Donwload');
                    }
                  }, {
                    text: 'Continuar',
                    handler: () => {
                        this.downloadPDF();
                        // console.log('Confirm Okay');
                    }
                  }
                ]
              });
      
              await alert.present();
        }
    }

    showLoader() {
        this.loaderToShow = this.loadingController.create({
            message: 'Aguarde por favor'
        }).then((res) => {
            res.present();

            res.onDidDismiss().then((dis) => {
            // console.log('Loading dismissed!');
            });
        });
        this.hideLoader();
    }

    hideLoader() {
        setTimeout(() => {
            this.loadingController.dismiss();
        }, 2000);
    }

    downloadPDF() {
        this.showLoader();

        /*
        const path = this.file.dataDirectory;
        const transfer = this.fileTransfer.create();

        transfer.download(this.downloadUrl, path + `${this.fileName}.pdf`)
            .then(entry => {
                const url = entry.toURL();
                this.fileSavedUrl.emit(url);
            });
        */
        this.file.checkDir(this.file.dataDirectory, this.folder).then (response => {
                // this.presentToast('repuesta: ' + JSON.stringify(response), 0);
                // if exist path
                if (response) {
                    this.folderExistDownload();
                // if not exist path
                } else {
                    this.createFolderAndDownload();
                }
            }).catch(err => {
            // this.presentToast('Error 5: ' + JSON.stringify(err), 1);
            this.createFolderAndDownload();
        });
    }

    folderExistDownload() {
        const transfer = this.fileTransfer.create();
        transfer.download(this.downloadUrl, this.file.dataDirectory + this.folder + '/' +
            `${this.fileName}.pdf`).then((entry) => {
            const url = entry.toURL();
            this.fileSavedUrl.emit(this.file.dataDirectory + this.folder + '/' + `${this.fileName}.pdf` );
            this.hideLoader();
            // this.presentToast(url, 1);
        }).catch((err) => {
            this.presentToast('Error 1: ' + JSON.stringify(err), 1);
        });
    }

    createFolderAndDownload() {
        this.file.createDir(this.file.dataDirectory, 'concensus', false).then(response2 => {
            // console.log('Directory created', response);
            const transfer = this.fileTransfer.create();
            transfer.download(this.downloadUrl, this.file.dataDirectory + this.folder + '/' +
                `${this.fileName}.pdf`).then((entry) => {
                const url = entry.toURL();
                // this.presentToast('Error 2: ' + JSON.stringify(url), 0);
                this.hideLoader();
                this.fileSavedUrl.emit(url);
            })
                .catch((err) => {
                    this.presentToast('Error 3: ' + JSON.stringify(err), 1);
                });

        }).catch(err => {
            this.presentToast('Error 4: ' + JSON.stringify(err), 1);
        });
    }

    async presentToast(msg: string, state: number) {
        let colorAux = 'success';
        if (state === 1) {
            colorAux = 'danger';
        }
        const toast = await this.toastController.create({
            message: msg,
            color: colorAux,
            // showCloseButton: true,
            // closeButtonText: 'Cerrar',
            animated: true,
            duration: 2000
        });
        toast.present();
    }

    ngOnDestroy(){
        // watch network for a disconnection
        let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
            console.log('network was disconnected :-(');
        });

        // stop disconnect watch
        disconnectSubscription.unsubscribe();
    }
}
