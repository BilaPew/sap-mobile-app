export class Authentication {
    constructor(
        public token?: string,
        public username?: string
    ) {
    }
}
