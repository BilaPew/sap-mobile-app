import {Component, Input, OnInit} from '@angular/core';
import {Platform, ToastController} from '@ionic/angular';
import {DocumentViewerOptions} from '@ionic-native/document-viewer';
import {FileOpener} from '@ionic-native/file-opener/ngx';
import {DocumentViewer} from '@ionic-native/document-viewer/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-pdf-viewer',
  templateUrl: './pdf-viewer.component.html',
  styleUrls: ['./pdf-viewer.component.scss'],
})
export class PdfViewerComponent implements OnInit {

    @Input()
    fileName: string;
    @Input()
    folder: string;

    constructor(
        private platform: Platform,
        private fileOpener: FileOpener,
        private file: File,
        private documentViewer: DocumentViewer,
        public toastController: ToastController
    ) { }

    ngOnInit() {}

    viewPDF() {
        if (this.platform.is('android')) {
            this.fileOpener.open(this.file.dataDirectory + this.folder + '/' + this.fileName + '.pdf', 'application/pdf')
                .then(() => {/* console.log('File is opened') */} )
                .catch(e => this.presentToast('Error al abrir el archivo: ' + JSON.stringify(e), 1));
        } else {
            // Use Document viewer for iOS for a better UI
            const options: DocumentViewerOptions = {
                title: 'My PDF'
            };
            this.documentViewer.viewDocument(this.file.dataDirectory + this.folder + '/' + this.fileName
                + '.pdf', 'application/pdf', options);
        }
    }

    async presentToast(msg: string, state: number) {
        let colorAux = 'success';
        if (state === 1) {
            colorAux = 'danger';
        }
        const toast = await this.toastController.create({
            message: msg,
            color: colorAux,
            // showCloseButton: true,
            // closeButtonText: 'Cerrar',
            animated: true,
            duration: 1500
        });
        toast.present();
    }
}
