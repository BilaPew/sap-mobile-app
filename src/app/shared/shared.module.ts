import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationService } from './services/authentication.service';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { FileDownloaderComponent } from './file-downloader/file-downloader.component';
import {FileOpener} from '@ionic-native/file-opener/ngx';
import { Network } from '@ionic-native/network/ngx';
import {DocumentViewer} from '@ionic-native/document-viewer/ngx';
import { PdfViewerComponent } from './pdf-viewer/pdf-viewer.component';

@NgModule({
  declarations: [ FileDownloaderComponent, PdfViewerComponent ],
    providers: [
        AuthenticationService,
        File,
        FileTransfer,
        FileOpener,
        Network,
        DocumentViewer
    ],
    imports: [
      CommonModule,
      IonicModule,
      FormsModule
    ],
    exports: [
      FileDownloaderComponent, PdfViewerComponent
    ]
})
export class SharedModule { }
