import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController, Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import {Authentication} from '../models/authentication';
import {error} from 'util';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

    authState = new BehaviorSubject(false);

    constructor(
        private router: Router,
        private storage: Storage,
        private platform: Platform,
        public toastController: ToastController
    ) {
        this.platform.ready().then(() => {
            this.ifLoggedIn();
        });
    }

    ifLoggedIn() {
        this.storage.get('USER_INFO').then((response) => {
            if (response) {
                this.authState.next(true);
            }
        });
    }

    // Fake login
    login(credentials: { username: string, password: string }): boolean {
        const dummyResponse: Authentication = {
            token: 'fake-token-generated',
            username: 'admin'
        };

        if (credentials.username === 'admin' && credentials.password === 'admin') {
            this.storage.set('USER_INFO', dummyResponse).then((response) => {
                this.router.navigate(['home']);
                this.authState.next(true);
            });
            return true;
        } else {
            return false;
        }
    }

    /* real login method
    login(credentials: { username: string, password: string, rememberMe: boolean }) {
        return this._httpClient
            .post<any>(`${this.url}`, credentials)
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    this.storeAuthenticationToken(user, credentials.rememberMe);
                    this.currentUserSubject.next(user);
                    this.timeOutService.setUserLogInTime();
                }
                return user;
            }));
    }*/



    logout() {
        this.storage.remove('USER_INFO').then(() => {
            this.router.navigate(['home']);
            this.authState.next(false);
        });
    }

    isAuthenticated() {
        return this.authState.value;
    }
}
