import {Component, OnDestroy} from '@angular/core';

import {MenuController, Platform} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthenticationService } from './shared/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnDestroy {

    appPages = [
        {
            title: 'Concensos',
            url: '/concensus-list',
            icon: 'list',
            loginRequired: false
        },
        {
            title: 'Configuracion',
            url: '/config',
            icon: 'settings',
            loginRequired: true
        }
    ];
    loggedIn = false;
    dark = false;

    subscription = this.authenticationService.authState.subscribe(
        authState => this.updateLoggedInStatus(authState));

    constructor(
    private router: Router,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authenticationService: AuthenticationService,
    private menu: MenuController
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();

            /* Ya no es obligatorio que el usuario este logueado para ver cosas
            this.authenticationService.authState.subscribe(state => {
                if (state) {
                    this.router.navigate(['home']);
                } else {
                    this.router.navigate(['login']);
                }
            });*/
        });
    }

    updateLoggedInStatus(loggedIn: boolean) {
        setTimeout(() => {
            this.loggedIn = loggedIn;
            // console.log('se logueo ' + this.loggedIn);

        }, 300);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    logOutUser() {
        this.authenticationService.logout();
    }
}
